#!/usr/bin/env python3

from mpi4py import MPI
import numpy as np
import sys
import time

#my libraries here

sys.path.insert(1,'.')
import pyGOL
import processGOL

#parameters

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

N = 10
gen = 10
start_gen = 1
#seed = (int(time.time()/10))%(420 + 69)
seed = 69 + 420
part_type = "metis"

if len(sys.argv) > 1:
    N = int(sys.argv[1])
if len(sys.argv) > 2:
    gen = int(sys.argv[2])
if len(sys.argv) > 3:
    part_type = sys.argv[3]

myTag = '_'+str(N)+'_'+str(size)+'_'+part_type

if rank == 0:
    print()
    print("Welcome to Donald's Game of Life!")
    print("Starting with...")
    print("Grid size = "+str(N))
    print("Partitioning scheme = "+part_type)
    print("Number of partitions = "+str(size))
    print("Number of generations = "+str(gen))
    print("Seed = "+str(seed))
    print()
    sys.stdout.flush()

#make the board

vals = np.array([1,0], dtype=np.int32)
np.random.seed(seed)
board = np.random.choice(vals, (N*N), p=[0.15,0.85])

'''debug stuff'''

'''board = np.array([[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,1,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]], dtype=np.int32)
for i in range(N*N):
    board[i] = np.int32(0)
board = board.reshape(N,N)
for j in range(5):
    i = j*20
    board[10+i][11+i] = 1
    board[10+i][12+i] = 1
    board[11+i][10+i] = 1
    board[11+i][11+i] = 1
    board[11+i][13+i] = 1
    board[11+i][14+i] = 1
    board[11+i][15+i] = 1
    board[11+i][16+i] = 1
    board[12+i][11+i] = 1
    board[12+i][12+i] = 1
    board[12+i][13+i] = 1
    board[12+i][14+i] = 1
    board[12+i][15+i] = 1
    board[12+i][16+i] = 1
    board[13+i][12+i] = 1
    board[13+i][13+i] = 1
    board[13+i][14+i] = 1
    board[13+i][15+i] = 1
board = board.flatten()'''

#make the partitions from pyGOL.py

if rank == 0:
    print("Initializing partitions...")
    sys.stdout.flush()
myPart = pyGOL.Partition(comm, N, board, myTag)
if rank == 0:
    print("Partitions initialized!")
    sys.stdout.flush()

#do the timesteps and write to bde files

if rank == 0:
    print("Starting the simulation...")
    sys.stdout.flush()
for i in range(gen):
    if (int(gen/2)==i) and rank == 0:
        print("Halfway probably...")
        sys.stdout.flush()
    if i >= start_gen:
        myPart.write_to_file(i, myTag)
    myPart.update_grid()

t = myPart.get_time()
time_buff = np.empty(1, dtype=np.float64)
time_buff[0] = np.float64(t)
if rank == 0:
    for i in range(1, size):
        comm.Recv(time_buff, source=i)
        t += float(time_buff[0])
else:
    comm.Send(time_buff, dest=0)

if rank == 0:
    total_time = t/size
    print("Done!")
    print("Time average across ranks = "+str(total_time))
    print("Average time per timestep = "+str(total_time/gen))
    print("Average lattice updates per second = "+str(int( (N*N) / (total_time/gen) ) ) )
    print()
    sys.stdout.flush()

#post process

processGOL.process(N, start_gen, gen, myTag)
