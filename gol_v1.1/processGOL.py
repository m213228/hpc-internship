import math
import os
import numpy as np
import sys
from mpi4py import MPI

sys.path.insert(1,'.')
from hdf5ForGoL import *

def process(N, start_gen, GEN, myTag):
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    order_map = np.fromfile("./ordermap"+myTag+".bde", dtype=np.int32)

    f = open("ranks"+myTag+".gol", 'r')
    rankGrid = np.empty((N*N), dtype=np.int32)
    for i in range(N*N):
        rankGrid[i] = np.int32(f.readline())
    f.close()

    if rank == 0:
        print("Starting visual file construction...")
        sys.stdout.flush()

    for i in range(start_gen, GEN):
        if i%size == rank:
            grid = np.fromfile("./dat/dat"+myTag+"_"+str(i)+".bde", dtype=np.int32)
            stateGrid = np.empty((N*N), dtype=np.int32)
            for j in range(N*N):
                stateGrid[order_map[j]] = grid[j]
            sh = "gol"+myTag+"_"+str(i)+".h5"
            sx = "gol"+myTag+"_"+str(i)+".xmf"
            dims = (1, N, N)
            writeH5(stateGrid, rankGrid, sh)
            writeXdmf(dims, N, sx, sh)

    if rank == 0:
        print("Done!")
        sys.stdout.flush()
