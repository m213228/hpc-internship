import numpy as np
import time
import gc
from mpi4py import MPI

class Partition(object):

    def __init__(self, comm, N, board, myTag):
        self.comm = comm
        self.size = comm.Get_size()
        self.rank = comm.Get_rank()

        f = open("ranks"+myTag+".gol", 'r')
        part_scheme = np.empty((N*N), dtype=np.int32)
        local_count = 0
        for i in range(N*N):
            part_scheme[i] = np.int32(f.readline())
            if part_scheme[i] == self.rank:
                local_count += 1
        f.close()
        board = board.reshape(N,N)
        part_scheme = part_scheme.reshape(N,N)
        lid = 0
        self.nbrsDict = {}
        self.localDict = {}
        self.globlDict = {}
        ex = [-1,0,1,-1,1,-1,0,1]
        ey = [-1,-1,-1,0,0,1,1,1]
        for i in range(N):
            for j in range(N):
                if self.rank == part_scheme[i][j]:
                    gid = j + (i*N)
                    for spd in range(len(ey)):
                        dx = ex[spd]; dy = ey[spd];
                        tx = (i+dx)%N; ty = (j+dy)%N;
                        tid = ty+tx*N
                        self.nbrsDict.setdefault(gid,[]).append(tid)
                    self.localDict[lid] = gid
                    self.globlDict[gid] = lid
                    lid += 1

        #determine outer nodes

        self.outerDict = {}
        self.outerCells = 0
        for i in range(local_count):
            for j in self.nbrsDict[self.localDict[i]]:
                if j in self.globlDict:
                    self.outerDict[i] = 0
                else:
                    self.outerDict[i] = 1
                    self.outerCells += 1
                    break
        part_scheme = part_scheme.flatten()
        self.nranksDict = {}
        for i in range(local_count):
            if self.outerDict[i] == 1:
                for j in range(8):
                    idx = self.nbrsDict[self.localDict[i]][j]
                    self.nranksDict.setdefault(i,[]).append(part_scheme[idx])

        for i in self.nranksDict:
            self.nranksDict[i] = list(dict.fromkeys(self.nranksDict[i]))
            self.nranksDict[i].remove(self.rank)

        #determine halo

        halo_list = []
        for i in range(local_count):
            if self.outerDict[i] == 1:
                gid = self.localDict[i]
                for j in range(8):
                    halo_list.append(self.nbrsDict[gid][j])

        halo_list = list(dict.fromkeys(halo_list))
        for i in range(local_count):
            if self.localDict[i] in halo_list:
                halo_list.remove(self.localDict[i])
        self.halo_num = len(halo_list)
        hidx = local_count
        self.rec_buff = []
        self.rec_buff_dict = {}
        hrbdc = 0
        for i in halo_list:
            self.globlDict[i] = hidx
            self.localDict[hidx] = i
            self.rec_buff.append(np.empty(1, dtype=np.int32))
            self.rec_buff_dict[hrbdc] = hidx
            hidx += 1
            hrbdc += 1


        #assign states to the grid

        board = board.flatten()
        self.grid = np.empty( (local_count+self.halo_num), dtype=np.int32)
        for i in range(N*N):
            if self.rank == part_scheme[i]:
                lid = self.globlDict[i]
                self.grid[lid] = board[i]

        self.recDict = {}
        for i in range(self.halo_num):
            self.recDict[halo_list[i]] = part_scheme[halo_list[i]]

        self.local_num = local_count

        #determine offset and order map

        temp = np.split(self.grid, [local_count])
        temp = temp[0]
        buff = np.empty(1, dtype=int)
        if not self.rank == 0:
            comm.Recv(buff, source=self.rank-1)
        self.offset = buff[0]
        buff[0] += temp.nbytes
        if not self.rank == (self.size-1):
            comm.Send(buff, dest=self.rank+1)
        del temp

        del board
        del part_scheme
        gc.collect()
        order_map = np.empty(local_count, dtype=np.int32)
        for i in range(local_count):
            order_map[i] = np.int32(self.localDict[i])

        amode = MPI.MODE_WRONLY|MPI.MODE_CREATE
        fh = MPI.File.Open(self.comm, "./ordermap"+myTag+".bde", amode)
        fh.Write_at_all(self.offset, order_map)
        fh.Close()

        del order_map
        del fh

        self.myTime = 0

    def next_state(self, i):
        total = 0
        next_i = self.grid[i]
        n_list = self.nbrsDict[self.localDict[i]].copy()
        for j in range(len(n_list)):
            n_list[j] = self.globlDict[n_list[j]]
            total += int(self.grid[n_list[j]])
        del n_list
        if self.grid[i] == 1:
            if (total < 2) or (total > 3):
                next_i = 0
        else:
            if total == 3:
                next_i = 1
        return np.int32(next_i)

    def update_grid(self):
        s_time = time.time()
        nextGrid = np.empty((self.local_num+self.halo_num), dtype=np.int32)
        req_list = []

        '''send my outer nodes'''
        for i in self.nranksDict:
            r_list = self.nranksDict[i]
            gid = self.localDict[i]
            buff = np.empty(1, dtype=np.int32)
            buff[0] = self.grid[i]
            for j in r_list:
                req_list.append(self.comm.Isend(buff, dest=j, tag=gid))

        '''receive my halo nodes'''

        buff_count = 0
        for i in self.recDict:
            req_list.append(self.comm.Irecv(self.rec_buff[buff_count],
                            source=self.recDict[i], tag=i))
            buff_count += 1

        '''calculate my inner nodes'''

        for i in range(self.local_num):
            if self.outerDict[i] == 0:
                nextGrid[i] = self.next_state(i)

        '''wait for comms to finish'''

        for i in req_list:
            i.Wait()
        del req_list
        for i in range(len(self.rec_buff)):
            self.grid[self.rec_buff_dict[i]] = self.rec_buff[i][0]

        '''calculate my outer nodes'''

        for i in range(self.local_num):
            if self.outerDict[i] == 1:
                nextGrid[i] = self.next_state(i)

        '''assign next grid to grid'''

        del self.grid
        self.grid = nextGrid

        s_time = time.time() - s_time
        self.myTime += s_time

    def write_to_file(self, gen, myTag):
        to_write = np.split(self.grid, [self.local_num])
        to_write = to_write[0]
        amode = MPI.MODE_WRONLY|MPI.MODE_CREATE
        fh = MPI.File.Open(self.comm, "./dat/dat"+myTag+"_"+str(gen)+".bde", amode)
        fh.Write_at_all(self.offset, to_write)
        fh.Close()
        del to_write
        del fh

    def get_time(self):
        return self.myTime


'''testing'''

if __name__=="__main__":
    seed = 1
    N = 10
    vals = np.array([1,0], dtype=np.int32)
    np.random.seed(seed)
    board = np.random.choice(vals, (N*N), p=[0.15,0.85])
    p = Partition(MPI.COMM_WORLD, N, board)
    grid = np.fromfile("./thisone.bde", dtype=np.int32)
    order_map = np.fromfile("./ordermap.bde", dtype=np.int32)
    real_grid = np.empty((N*N), dtype=np.int32)
    for i in range(N*N):
        real_grid[order_map[i]] = grid[i]
    grid = real_grid
    p.update_grid()
