#!/usr/bin/env python3

import numpy as np
import pymetis as pmt
import gc
import sys
import math

def set_adjacency(N):
    adjDict = {}
    ex = [-1., 0., 1.,-1., 1.,-1., 0., 1.]
    ey = [-1.,-1.,-1., 0., 0., 1., 1., 1.]
    for i in range(N):
        for j in range(N):
            gid = j + i*N
            for spd in range(len(ex)):
                dx = int(ex[spd]); dy = (ey[spd]);
                tx = (i+dx)%N; ty = (j+dy)%N;
                tid = ty+tx*N
                adjDict.setdefault(gid,[]).append(tid)

    return adjDict

def part_1d(N, size):
    rows_per_rank = int(N/size)
    extras = int(N%size)
    t_grid = np.empty((N*N), dtype=np.int32)
    idx = 0
    for i in range(size):
        for j in range(rows_per_rank):
            for k in range(N):
                t_grid[idx] = i
                idx += 1
    for i in range(extras):
        for i in range(N):
            t_grid[idx] = size-1
            idx += 1
    return t_grid

def part_2d(N, size):
    fac1 = 1
    for i in range(2, int(math.sqrt(size))+1):
        if size % i == 0:
            fac1 = i
    fac2 = int(size/fac1)
    H = fac1
    W = fac2
    print("Using "+str(H)+" by "+str(W)+" scheme...")
    sys.stdout.flush()
    t_grid = np.empty((N*N), dtype=np.int32)
    rows_per_rank = int(N/H)
    extra_rows = int(N%H)
    cols_per_rank = int(N/W)
    extra_cols = int(N%W)
    idx = 0
    r_grid = np.empty(size, np.int32)
    for i in range(size):
        r_grid[i] = i
    idx = 0
    for i in range(H):
        for j in range(rows_per_rank):
            for k in range(W):
                cur_rank = np.int32(k + i*W)
                for l in range(cols_per_rank):
                    t_grid[idx] = cur_rank
                    idx += 1
                if cur_rank%W == W - 1:
                    for m in range(extra_cols):
                        t_grid[idx] = cur_rank
                        idx += 1
        if i == H-1:
            for j in range(extra_rows):
                for k in range(W):
                    cur_rank = np.int32(k + i*W)
                    for l in range(cols_per_rank):
                        t_grid[idx] = cur_rank
                        idx += 1
                    if cur_rank%W == W - 1:
                        for m in range(extra_cols):
                            t_grid[idx] = cur_rank
                            idx += 1


    return t_grid

def partition(N, size):
    print("Writing partition scheme...")
    sys.stdout.flush()
    myTag = '_'+str(N)+'_'+str(size)+'_'
    adjDict = set_adjacency(N)
    print("Starting metis...")
    sys.stdout.flush()
    cuts, grid = pmt.part_graph(size, adjDict)
    del adjDict
    gc.collect()
    print("Metis finished with "+str(cuts)+" cuts")
    f = open('ranks'+myTag+'metis'+'.gol', 'w')
    for i in grid:
        s = str(i) + '\n'
        f.write(s)
    f.close()
    del grid
    sys.stdout.flush()
    print("Getting 2D partitioning...")
    sys.stdout.flush()
    grid = part_2d(N, size)
    f = open('ranks'+myTag+'2d'+'.gol', 'w')
    for i in grid:
        s = str(i) + '\n'
        f.write(s)
    f.close()
    del grid
    print("Getting 1D partitioning...")
    sys.stdout.flush()
    grid = part_1d(N, size)
    f = open('ranks'+myTag+'1d'+'.gol', 'w')
    for i in grid:
        s = str(i) + '\n'
        f.write(s)
    f.close()
    del f
    del grid
    gc.collect()



if __name__=="__main__":
    N = 10
    size = 8
    part_type = 'metis'
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    if len(sys.argv) > 2:
        size = int(sys.argv[2])

    partition(N, size)
