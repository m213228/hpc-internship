#!/usr/bin/env python3

import math
import os
import numpy as np
from mpi4py import MPI
from hdf5ForGoL import *


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

param_file = open("params.bde",'r')
N = int(param_file.readline())
GEN = int(param_file.readline())
size = int(param_file.readline())
rows_per_rank = int(param_file.readline())

for i in range(GEN):
    if i%size == rank:
        grid = np.fromfile("./dat/dat_"+str(i)+".bde", dtype=np.int32)
        stateGrid = np.empty((N*N), dtype=np.int32)
        rankGrid = np.empty((N*N), dtype=np.int32)
        cur_rank = 0
        cur_row = 0
        cur_col = 0
        for j in range(N*N):
            stateGrid[j] = grid[j]
            rankGrid[j] = cur_rank
            cur_col += 1
            if cur_col==N:
                cur_col = 0
                cur_row += 1
                if  cur_row==rows_per_rank:
                    cur_row = 0
                    cur_rank += 1
        sh = "gol_1d_"+str(i)+".h5"
        sx = "gol_1d_"+str(i)+".xmf"
        dims = (1, N, N)
        writeH5(stateGrid, rankGrid, sh)
        writeXdmf(dims, N, sx, sh)
