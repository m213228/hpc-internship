#!/usr/bin/env python3

import numpy as np
import time
import array
import sys
from mpi4py import MPI

N = 64
vals = np.array([1, 0], np.int32)
GEN = 50

if len(sys.argv) > 1:
    N = int(sys.argv[1])
if len(sys.argv) > 2:
    GEN = int(sys.argv[2])

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
rows_per_rank = int(N/size)
seed = 420 + 69 + rank

if len(sys.argv) > 3:
    seed = rank + int(sys.argv[3])

if not (N%size==0):
    if rank==0:
        print("Invalid grid size or processes")
    sys.exit()

np.random.seed(seed)
grid = np.random.choice(vals, rows_per_rank*N, p=[0.15, 0.85]).reshape(rows_per_rank, N)
'''grid = np.array([[0,0,0,1,1,0,0,0],
                [0,1,0,1,1,1,0,0],
                [1,1,1,0,1,1,0,0],
                [0,1,0,0,0,0,0,0]])'''
'''for i in range(rows_per_rank):
    for j in range(N):
        grid[i][j]+= rank*10'''
newGrid = grid

if rank == 0:
    f = open('params.bde', 'w')
    s = str(N)+'\n'+str(GEN)+'\n'+str(size)+'\n'+str(rows_per_rank)+'\n'
    f.write(s)
    f.close()

def write_to_file(gen):
    amode = MPI.MODE_WRONLY|MPI.MODE_CREATE
    fh = MPI.File.Open(comm, "./dat/dat_"+str(gen)+".bde", amode)
    offset = rank*grid.nbytes
    fh.Write_at_all(offset, grid)
    fh.Close()
    
def check_row(top, mid, bot):
    newMid = mid.copy()
    for j in range(N):
        total = (top[(j-1)%N] + top[j] +
                 top[(j+1)%N] + mid[(j-1)%N] +
                 mid[(j+1)%N] + bot[(j-1)%N] +
                 bot[j] + bot[(j+1)%N])
        if mid[j] == 1:
            if (total < 2) or (total > 3):
                newMid[j] = 0
        else:
            if total == 3:
                newMid[j] = 1
    return newMid

def update():
    global grid, newGrid
    newGrid = grid.copy()

    '''send top and bottom rows to neighbors'''

    comm.Isend(grid[0], dest=(rank-1)%size)
    comm.Isend(grid[rows_per_rank-1], dest=(rank+1)%size)

    '''calculate inner states'''

    for i in range(1, rows_per_rank-1):
        newGrid[i] = check_row(grid[i-1], grid[i], grid[i+1])

    '''receive borders from neighbors'''
    bot_ghost = grid[0].copy() 
    top_ghost = grid[0].copy()

    req1=comm.Irecv(bot_ghost, source=(rank+1)%size)
    req2=comm.Irecv(top_ghost, source=(rank-1)%size)
    req1.Wait()
    req2.Wait()

    '''calc border states'''

    newGrid[0] = check_row(top_ghost, grid[0], grid[1])
    q = rows_per_rank-1
    newGrid[q] = check_row(grid[q-1], grid[q], bot_ghost)

    '''wait'''

    comm.Barrier()

    grid = newGrid


if rank==0:
    print("Starting the thing...")
    sys.stdout.flush()

for v in range(GEN):
    if (rank==0) and (v==GEN/2):
        print("The thing is like halfway through or something...")
        sys.stdout.flush()
    write_to_file(v)
    update()


if rank==0:
    print("The thing is finished!")

