import numpy as np
import array
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpi4py import MPI

x = np.fromfile("./dat/dat_3.bde", dtype=np.int32).reshape(20,20)
for i in range(20):
    for j in range(20):
        x[i][j] %= 2

print(str(x))
