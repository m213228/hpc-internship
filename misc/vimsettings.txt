" enable syntax highlighting
syntax enable
filetype plugin on
filetype indent on

" set tabs to have 4 spaces
set ts=4
set smarttab

" indent when moving to the next line while writing code
set autoindent
set smartindent

" expand tabs into spaces
set expandtab

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" show the matching part of the pair for [] {} and ()
set showmatch
set foldcolumn=1

" enable all Python syntax highlighting features
let python_highlight_all = 1

